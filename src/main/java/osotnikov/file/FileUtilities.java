package osotnikov.file;

import org.apache.commons.io.IOUtils;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.nio.charset.StandardCharsets;

public class FileUtilities {

	/**
	 * Will return null if argument is null, the contents of the resource as a String otherwise.
	 * 
	 * */
	public static String getUtf8ResourceAsString(String pathAsString) throws IOException {
		
		if(pathAsString == null) {
			return null;
		}
		
		URL resource = FileUtilities.class.getResource(pathAsString);
        return IOUtils.toString(resource, StandardCharsets.UTF_8);
    }

	/**
	 * Will return null if argument is null, the contents of the resource as an InputStream otherwise.
	 *
	 * */
	public static InputStream getResourceAsInputStream(String pathAsString) {

		if(pathAsString == null) {
			return null;
		} else if(pathAsString.startsWith("/") || pathAsString.startsWith("\\")){
			pathAsString = pathAsString.substring(1);
		}

		return (new FileUtilities()).getClass()
				.getClassLoader().getResourceAsStream(pathAsString);
	}
	
}
