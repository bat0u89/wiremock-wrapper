package osotnikov.wiremock.utils;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.client.ResponseDefinitionBuilder;

import static com.github.tomakehurst.wiremock.client.WireMock.*;

public class MockBuilderHelper {
	
	private String urlPath = "/a-path";
	
	private String requestAsString = null;
	private String responseAsString = null;
	private int responseHttpStatus = 200;

	public MappingBuilder createMockBuilderForResponseToFileUploadRequest() {

		return createMockBuilderForFileUploadRequest()
				.willReturn(createMockBuilderForFileUploadResponse());
	}

	public MappingBuilder createMockBuilderForFileUploadRequest() {

		return any(urlPathEqualTo(urlPath))
				.withHeader("Content-Type", containing("multipart/form-data"))
				.withMultipartRequestBody(aMultipart()
						.withName("inputFile")
						.withBody(containing(getRequestAsString())))
				.willReturn(ok());
	}

	public ResponseDefinitionBuilder createMockBuilderForFileUploadResponse() {

		return aResponse()
				.withStatus(responseHttpStatus)

				.withHeader("Content-Type", "application/json;charset=UTF-8")
				.withHeader("Transfer-Encoding", "chunked")
				.withHeader("Date", "Tue, 11 Feb 2020 17:32:32 GMT")

				.withBody(responseAsString);
	}

	public MappingBuilder createMockBuilderForXmlResponseToAnyRequest() {
    	
    	return createMockBuilderForAnyXmlRequest()
            .willReturn(createMockBuilderForXmlResponse());
    }
	
	public MappingBuilder createMockBuilderForXmlResponseToXmlRequest() {
    	
    	return createMockBuilderForXmlRequest()
            .willReturn(createMockBuilderForXmlResponse());
    }
	
	public MappingBuilder createMockBuilderForAnyXmlRequest() {
    	
    	return any(urlPathEqualTo(urlPath))
            .withHeader("Accept", containing("xml"));
    }
    
	public MappingBuilder createMockBuilderForXmlRequest() {
    	
    	return any(urlPathEqualTo(urlPath))
            .withHeader("Accept", containing("xml"))
            .withRequestBody(equalToXml(requestAsString));
    }
    
	public ResponseDefinitionBuilder createMockBuilderForXmlResponse() {
    	
    	return aResponse()
            .withStatus(responseHttpStatus)

            .withHeader("Cache-Control", "no-cache", "no-store", "max-age=0", "must-revalidate")
            .withHeader("Pragma", "no-cache")
            .withHeader("Expires", "0")
            .withHeader("X-XSS-Protection", "1; mode=block")
            .withHeader("X-Frame-Options", "DENY")
            .withHeader("X-Content-Type-Options", "nosniff")
            .withHeader("Accept", "text/xml")
            .withHeader("Content-Type", "text/xml;charset=utf-8")
            .withHeader("Connection", "close")

            .withBody(responseAsString);
    }

	public String getUrlPath() {
		return urlPath;
	}

	public void setUrlPath(String urlPath) {
		this.urlPath = urlPath;
	}

	public String getRequestAsString() {
		return requestAsString;
	}

	public void setRequestAsString(String requestAsString) {
		this.requestAsString = requestAsString;
	}

	public String getResponseAsString() {
		return responseAsString;
	}

	public void setResponseAsString(String responseAsString) {
		this.responseAsString = responseAsString;
	}

	public int getResponseHttpStatus() {
		return responseHttpStatus;
	}

	public void setResponseHttpStatus(int responseHttpStatus) {
		this.responseHttpStatus = responseHttpStatus;
	}
}
