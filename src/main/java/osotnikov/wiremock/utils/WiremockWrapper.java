package osotnikov.wiremock.utils;

import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URISyntaxException;

import osotnikov.file.FileUtilities;

public class WiremockWrapper {

	private MockBuilderHelper mockBuilderHelper = new MockBuilderHelper();

    public WiremockWrapper(String urlPath) {
		
		mockBuilderHelper.setUrlPath(urlPath);
	}

    public void setupMockServerCsvUploadRequestAndResponse(String pathToCsv, String pathToResponse, int responseHttpStatus) throws IOException, URISyntaxException {

        setRequestAndResponseFromResources(pathToCsv, pathToResponse, responseHttpStatus);
        stubAMockFileUploadEndpointWithResponseForProvidedRequest();
    }

    private void stubAMockFileUploadEndpointWithResponseForProvidedRequest() {

        stubFor(mockBuilderHelper.createMockBuilderForResponseToFileUploadRequest());
    }


    public void setupMockServerXmlRequestAndResponse(String pathToRequestXml, String pathToResponseXml, int responseHttpStatus) throws UnsupportedEncodingException, URISyntaxException, IOException {

    	setRequestAndResponseFromResources(pathToRequestXml, pathToResponseXml, responseHttpStatus);
    	stubAMockSoapEndpointXmlResponseForProvidedXmlRequest(responseHttpStatus);
    }
    
    public void setupMockServerXmlResponseForAnyRequest(String pathToResponseXml, int responseHttpStatus) throws UnsupportedEncodingException, URISyntaxException, IOException {

    	setResponseFromResource(pathToResponseXml, responseHttpStatus);
    	stubAMockXmlResponseForAnyRequest(responseHttpStatus);
    }
    
    private void stubAMockXmlResponseForAnyRequest(int responseHttpStatus) {

        stubFor(
        	mockBuilderHelper.createMockBuilderForXmlResponseToAnyRequest());
    }
    
    private void stubAMockSoapEndpointXmlResponseForProvidedXmlRequest(int responseHttpStatus) {

        stubFor(
        	mockBuilderHelper.createMockBuilderForXmlResponseToXmlRequest());
    }
    
    private void setRequestFromResource(String pathToRequest) throws UnsupportedEncodingException, URISyntaxException, IOException {
    	
    	mockBuilderHelper.setRequestAsString(FileUtilities.getUtf8ResourceAsString(pathToRequest));
    }
    
    private void setResponseFromResource(String pathToResponse, int responseHttpStatus) throws UnsupportedEncodingException, URISyntaxException, IOException {
    	
    	mockBuilderHelper.setResponseHttpStatus(responseHttpStatus);
    	mockBuilderHelper.setResponseAsString(FileUtilities.getUtf8ResourceAsString(pathToResponse));
    }
    
    private void setRequestAndResponseFromResources(String pathToRequest, String pathToResponse, int responseHttpStatus) throws UnsupportedEncodingException, URISyntaxException, IOException {
    	
    	setRequestFromResource(pathToRequest);
    	setResponseFromResource(pathToResponse, responseHttpStatus);
    }
	
}
